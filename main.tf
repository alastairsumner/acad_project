provider "aws" {
    access_key = var.access_key
    secret_key = var.secret_key
    token = var.token
    region = var.region
}

module "elb_http" {
  source  = "terraform-aws-modules/elb/aws"
  version = "~> 2.0"

  name = "alastair-elb"

  subnets         = [ aws_subnet.alastair_VPC_Subnet.id ]
  security_groups = [ aws_security_group.alastair_VPC_Security_Group.id ]
  internal        = false

  listener = [
    {
      instance_port     = "80"
      instance_protocol = "HTTP"
      lb_port           = "80"
      lb_protocol       = "HTTP"
    },
    {
      instance_port     = "8080"
      instance_protocol = "http"
      lb_port           = "8080"
      lb_protocol       = "http"
    },
  ]

  health_check = {
    target              = "HTTP:80/"
    interval            = 30
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 5
  }

  number_of_instances         = var.num_webs
  instances                   = aws_instance.al_web[*].id
  cross_zone_load_balancing   = true
  idle_timeout                = 400
  connection_draining         = true
  connection_draining_timeout = 400

  tags = {
    Name = "alastair-terraform-elb"
  }
}
