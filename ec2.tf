# Create EC2 instances
resource "aws_instance" "al_web" {
    ami = var.ami
    instance_type = var.instance_type
    count = var.num_webs
    subnet_id = aws_subnet.alastair_VPC_Subnet.id
    vpc_security_group_ids = [ aws_security_group.alastair_VPC_Security_Group.id ]
    user_data = <<-EOF
                #! /bin/bash
                sudo yum -y update
                sudo yum install -y httpd
                sudo chmod 777 /var/www/html/
                sudo echo "Hello Al" > /var/www/html/index.html
                sudo systemctl start httpd
                sudo systemctl enable httpd
                EOF
    # connection {
    #     type     = "ssh"
    #     user     = "ec2-user"
    #     private_key = var.ami_keypair_name
    #     host     = self.public_ip
    # }
    tags = {
        "Name" = "alastair ${count.index + 1}/${var.num_webs}"
    }
}

output "public_ip" {
  value = aws_instance.al_web[*].public_ip
}

output "public_dns" {
  value = aws_instance.al_web[*].public_dns
}