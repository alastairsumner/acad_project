variable "access_key" {}
variable "secret_key" {}
variable "token" {}
variable "region" {
  default = "eu-west-1"
}

variable "ami" {
    default = "ami-014ce76919b528bff"
}
variable "instance_type" {
    default = "t2.micro"
}
variable "num_webs" {
    default = "3"
}

variable "availabilityZone" {
    default = "eu-west-1a"
}

variable "instanceTenancy" {
    default = "default"
}
variable "dnsSupport" {
    default = true
}
variable "dnsHostNames" {
    default = true
}
variable "vpcCIDRblock" {
    default = "10.0.0.0/16"
}
variable "subnetCIDRblock" {
    default = "10.0.1.0/24"
}
variable "destinationCIDRblock" {
    default = "0.0.0.0/0"
}
variable "ingressCIDRblock" {
    type = list
    default = [ "0.0.0.0/0" ]
}
variable "egressCIDRblock" {
    type = list
    default = [ "0.0.0.0/0" ]
}
variable "mapPublicIP" {
    default = true
}