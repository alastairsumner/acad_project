# create the VPC
resource "aws_vpc" "alastair_VPC" {
  cidr_block           = var.vpcCIDRblock
  instance_tenancy     = var.instanceTenancy 
  enable_dns_support   = var.dnsSupport 
  enable_dns_hostnames = var.dnsHostNames

  tags = {
      Name = "acad-alastair"
  }
} # end resource

# create the Subnet
resource "aws_subnet" "alastair_VPC_Subnet" {
  vpc_id                  = aws_vpc.alastair_VPC.id
  cidr_block              = var.subnetCIDRblock
  map_public_ip_on_launch = var.mapPublicIP 
  availability_zone       = var.availabilityZone

  tags = {
     Name = "alastair VPC Subnet"
  }
} # end resource

# Create the Security Group
resource "aws_security_group" "alastair_VPC_Security_Group" {
  vpc_id       = aws_vpc.alastair_VPC.id
  name         = "alastair VPC Security Group"
  description  = "alastair VPC Security Group"

# allow ingress of port 22
  ingress {
      from_port = 22
      to_port = 22
      protocol = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
  }

#   allow ingress of port 80
  ingress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  
  # allow egress of all ports
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "alastair VPC Security Group"
    Description = "alastair VPC Security Group"
  }
} # end resource

# create VPC Network access control list
resource "aws_network_acl" "alastair_VPC_Security_ACL" {
  vpc_id = aws_vpc.alastair_VPC.id
  subnet_ids = [ aws_subnet.alastair_VPC_Subnet.id ]
# allow ingress port 22
  ingress {
    protocol   = "tcp"
    rule_no    = 100
    action     = "allow"
    cidr_block = var.destinationCIDRblock 
    from_port  = 22
    to_port    = 22
  }
  
  # allow ingress port 80 
  ingress {
    protocol   = "tcp"
    rule_no    = 200
    action     = "allow"
    cidr_block = var.destinationCIDRblock 
    from_port  = 80
    to_port    = 80
  }
  
  # allow egress port 22 
  egress {
    protocol   = "tcp"
    rule_no    = 100
    action     = "allow"
    cidr_block = var.destinationCIDRblock
    from_port  = 22 
    to_port    = 22
  }
  
  # allow egress port 80 
  egress {
    protocol   = "tcp"
    rule_no    = 200
    action     = "allow"
    cidr_block = var.destinationCIDRblock
    from_port  = 80  
    to_port    = 80 
  }

  tags = {
      Name = "acad-alastair-acl"
  }
} # end resource

# Create the Internet Gateway
resource "aws_internet_gateway" "alastair_VPC_GW" {
 vpc_id = aws_vpc.alastair_VPC.id
 tags = {
    Name = "alastair VPC Internet Gateway"
 }
} # end resource

# Create the Route Table
resource "aws_route_table" "alastair_VPC_route_table" {
 vpc_id = aws_vpc.alastair_VPC.id
 tags = {
    Name = "alastair VPC Route Table"
 }
} # end resource

# Create the Internet Access
resource "aws_route" "alastair_VPC_internet_access" {
  route_table_id         = aws_route_table.alastair_VPC_route_table.id
  destination_cidr_block = var.destinationCIDRblock
  gateway_id             = aws_internet_gateway.alastair_VPC_GW.id
} # end resource

# Associate the Route Table with the Subnet
resource "aws_route_table_association" "alastair_VPC_association" {
  subnet_id      = aws_subnet.alastair_VPC_Subnet.id
  route_table_id = aws_route_table.alastair_VPC_route_table.id
} # end resource

output "subnet_id" {
    value = aws_subnet.alastair_VPC_Subnet.id
}

output "vpc_security_group_id" {
    value = aws_security_group.alastair_VPC_Security_Group.id
}
