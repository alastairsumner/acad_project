terraform {
  backend "remote" {
    organization = "alastairsumner"

    workspaces {
      name = "alastair-terraform"
    }
  }
}